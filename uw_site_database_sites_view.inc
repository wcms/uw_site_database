<?php

/**
 * @file
 * Functions related to viewing sites.
 */

/**
 *
 */
function uw_site_database_sites_view($category = NULL, $code = NULL) {
  // WCMS 3 migration status.
  $display = NULL;
  $params = [];
  $query = drupal_get_query_parameters();
  if (($query['sort'] ?? NULL) === 'wcms3_status') {
    $display = 'wcms3_status';
    $params['sort'] = ['wcms_site_status.sort IS NULL', 'wcms_site_status.sort'];
  }

  list($page, $title) = uw_site_database_sites_page($category, $code, $params);

  if ($display === 'wcms3_status') {
    $title .= ': WCMS 3 migration status';
  }

  drupal_set_title($title);

  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('UW Site Database'), 'uw_site_database');
  drupal_set_breadcrumb($breadcrumb);

  return $page;
}

/**
 * Return the page render array and title for a sites listing page.
 *
 * @param string $category
 *   "unit", "group", or NULL to not add a WHERE sub-clause by unit or group.
 * @param string $code
 *   The unit or group code.
 * @param array|null $params
 *   An array of parameters. The only supported key is 'sort', which is set as
 *   the $sort param to uw_site_database_sites_index_query().
 *
 * @return array
 *   An array with:
 *    - The page render array.
 *    - The page title.
 */
function uw_site_database_sites_page($category = NULL, $code = NULL, array $params = NULL) {
  $r = uw_site_database_sites_index_query($category, $code, NULL, [], $params['sort'] ?? NULL);
  if (!$r) {
    return ['', 'Error'];
  }

  $title = 'Web sites (' . $r->rowCount() . ')';

  $page = array();
  if (drupal_valid_path('uw_site_database/sites/edit/new')) {
    $page['new'] = array(
      '#markup' => '<p><a href="' . url('uw_site_database/sites/edit/new') . '">New site</a></p>',
    );
  }
  $page['list'] = array(
    '#theme' => 'uw_site_database_sites_index',
    '#query_results' => $r,
  );
  return array($page, $title);
}

/**
 * Query the sites database for a list of sites.
 *
 * @param string $category
 *   "unit", "group", or NULL to not add a WHERE sub-clause by unit or group.
 * @param string $code
 *   The unit or group code.
 * @param string|array|null $where
 *   An array of sub-clauses for the WHERE clause.
 * @param array $a
 *   An array of arguments for the prepared statement.
 * @param array|null $sort
 *   An array of sort parameters or NULL to sort by 'id'.
 *
 * @return DatabaseStatementInterface
 *   The query result object.
 */
function uw_site_database_sites_index_query($category = NULL, $code = NULL, $where = NULL, array $a = array(), array $sort = NULL) {
  $where = (array) $where;
  if (in_array($category, array('unit', 'group'))) {
    $where[] = '{uw_unit}.' . $category . '_code = :code';
    $a['code'] = $code;
  }
  if ($where) {
    $where = 'WHERE (' . implode(') AND (', $where) . ')';
  }
  else {
    $where = NULL;
  }
  $q = 'SELECT {web_site}.' . implode(', {web_site}.', array_keys(uw_site_database_get_fields_site())) . ', unit_short_name, COALESCE(group_full_name, group_short_name, \'Not part of a group\') AS group_name,
      (SELECT 1 FROM {site_drupal_project} WHERE {site_drupal_project}.site_path = wcms_path LIMIT 1) AS has_makefile
    FROM web_site
      LEFT JOIN {uw_unit} USING(unit_code)
      LEFT JOIN {uw_group} USING(group_code)
      LEFT JOIN {web_host} USING(host_name)
      LEFT JOIN {wcms_site_status} wcms_site_status ON (wcms3_status = wcms_site_status.status_id)
    ' . $where . '
    ORDER BY ';

  if ($sort) {
    $q .= implode(', ', $sort);
  }
  else {
    $q .= 'id';
  }

  return uw_site_database_query($q, $a);
}

/**
 *
 */
function theme_uw_site_database_sites_index(array $variables) {
  module_load_include('inc', 'uw_wcms_tools', 'devops/uw_devops');
  module_load_include('inc', 'uw_wcms_tools', 'uw_wcms_tools.lib');

  global $user;

  $query_results = $variables['query_results'];
  $rows = array();
  while ($row = $query_results->fetch()) {
    // This cast must not be done in the while().
    $row = (array) $row;

    // Add edit link if user has access.
    $url = 'uw_site_database/sites/edit/' . $row['id'];
    if (drupal_valid_path($url)) {
      $row['edit'] = '<a href="' . url($url) . '">edit</a>';
    }

    if ($row['unit_code']) {
      $title = $row['unit_short_name'];
      if ($row['group_name']) {
        $title .= ', ' . $row['group_name'];
      }
      $row['unit_code'] = '<abbr title="' . $title . '">' . $row['unit_code'] . '</abbr>';
    }
    else {
      $row['unit_code'] = '(no unit)';
    }
    unset($row['id']);
    unset($row['unit_short_name']);
    unset($row['unit_full_name']);
    unset($row['group_code']);
    unset($row['group_name']);
    unset($row['group_full_name']);
    unset($row['group_short_name']);

    if ($row['url_1']) {
      $row['site_name'] = l($row['site_name'], $row['url_1']);
    }
    elseif ($row['host_name']) {
      $row['site_name'] = l($row['site_name'], 'http://' . $row['host_name'] . '/');
    }
    unset($row['url_1']);
    unset($row['host_name']);

    if ($row['has_makefile']) {
      $repository_path = NULL;
      try {
        $repository_path = url_path_to_repository_path($row['wcms_path']);
      }
      catch (Exception $e) {
        drupal_set_message(t('Invalid path: @path', ['@path' => $row['wcms_path']]), 'error');
      }
      $link = uw_wcms_tools_get_project_url_by_namespace_and_path('wcms-sites', $repository_path);
      $row['has_makefile'] = l(t('Makefile'), $link);
    }

    if ($row['wcms_path']) {
      $row['wcms_path'] = l($row['wcms_path'], uw_site_database_get_wcms_url($row['wcms_path']));
    }

    if ($row['wcms_path_short']) {
      $row['wcms_path_short'] = l($row['wcms_path_short'], uw_site_database_get_wcms_url($row['wcms_path_short']));
    }

    if (isset($row['wcag_compliance_level'])) {
      $wcag_compliance_level_text = wcag_compliance_levels()[$row['wcag_compliance_level']];
      $row['wcag_compliance_level'] = [
        'class' => 'wcag-' . $row['wcag_compliance_level'],
        'data' => $wcag_compliance_level_text,
      ];
    }
    else {
      $row['wcag_compliance_level'] = [
        'class' => 'wcag-unknown',
        'data' => NULL,
      ];
    }

    $row['contact_email'] = uw_site_database_watiam_view_line($row['contact_email']);

    $row['rt'] = uw_site_database_rt_link($row['rt']);

    $row['wcms3_rt'] = uw_site_database_rt_link($row['wcms3_rt']);

    // Sort columns in desired order. List columns to bring to front. The rest
    // remain in their current order.
    $new_row = [];
    $sort = [
      'site_name',
      'edit',
      'has_makefile',
    ];
    foreach ($sort as $item) {
      if (array_key_exists($item, $row)) {
        $new_row[$item] = $row[$item];
        unset($row[$item]);
      }
    }
    $new_row += $row;
    $row = $new_row;
    unset($new_row);

    // Hide columns as desired.
    if ($variables['options'] === 'a11y') {
      unset($row['private']);
      unset($row['start_migration']);
      unset($row['end_migration']);
      unset($row['go_live']);
      unset($row['migration_meeting']);
      unset($row['private']);
      unset($row['owner_type']);
      unset($row['priority']);
      unset($row['migrator']);
      unset($row['has_makefile']);
    }
    // Hide for non-a11y page or anonnymous users.
    if ($variables['options'] !== 'a11y' || $user->uid === 0) {
      unset($row['wcag_compliance_level']);
      unset($row['wcag_compliance_work_required']);
      unset($row['wcag_compliance_check_date']);
    }

    $rows[] = [
      'class' => $row['status'] ? array($row['status']) : NULL,
      'data' => $row,
    ];
  }
  if (!empty($rows[0]) && is_array($rows[0])) {
    $headers = array();
    $header_names = uw_site_database_get_fields_site();
    foreach (array_keys($rows[0]['data']) as $key) {
      $header = isset($header_names[$key]) ? $header_names[$key] : ucfirst(str_replace('_', ' ', $key));

      if ($key === 'wcms3_status') {
        $header = '<a href="?sort=wcms3_status">' . $header . '</a>';
      }

      $headers[] = $header;
    }
    $variables = array(
      'header' => $headers,
      'rows' => $rows,
    );
    return theme('table', $variables);
  }
  else {
    return 'No sites found.';
  }
}
