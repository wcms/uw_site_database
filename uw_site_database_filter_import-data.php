<?php

/**
 * @file
 * CLI script for creating a list of hostnames from GSA data.
 *
 * Open all files in the import-data directory, assume they are Google Search
 * Appliance (GSA) export files, create a list of all the unique host names.
 */

$hosts = array();
$path = __DIR__ . '/import-data';
foreach (scandir($path) as $filename) {
  if (is_file($path . '/' . $filename)) {
    echo $filename . ":\n";
    $file = fopen($path . '/' . $filename, 'r');
    if (!$file) {
      echo "Error opening file.\n";
      continue;
    }

    while (($line = fgetcsv($file, NULL, "\t")) !== FALSE) {
      $host_name = strtolower($line[0]);
      $match = preg_match(',^[^:]+://(?:[^/]+@)?([.a-z0-9-]+)(?::\d+)?/,', $host_name, $backref);
      if (!$match) {
        echo 'Non-match: ' . $host_name . "\n";
        continue;
      }
      $hosts[$backref[1]] = TRUE;
    }
  }
}
$hosts = array_keys($hosts);
file_put_contents(__DIR__ . '/import-hosts.txt', implode("\n", $hosts));
