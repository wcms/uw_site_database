<?php

/**
 * @file
 * Functions relating to accessibility reports.
 */

/**
 * Page callback for the main accessibility report.
 *
 * @param string $category
 *   The category, either "unit" or "group".
 * @param string $code
 *   The unit or group code.
 *
 * @return array
 *   The page render array.
 */
function uw_site_database_a11y_index($category = NULL, $code = NULL) {
  module_load_include('inc', 'uw_site_database', 'uw_site_database_sites_view');
  $where = '(uw_unit.' . $category . "_code = :code OR web_host.unit_code = :code OR hosted_by_unit_code = :code)
    AND (status IS NULL OR status NOT IN ('done', 'culled'))
    -- Exclude new WCMS sites.
    AND NOT (status = 'migrating' AND host_name IS NULL AND url_1)";
  $a = ['code' => $code];

  $r = uw_site_database_sites_index_query(NULL, NULL, $where, $a);

  if (!$r) {
    return '';
  }

  $name = uw_site_database_org_name($category, $code);
  if ($name) {
    $name = 'in ' . $name;
  }
  else {
    $name = 'with no ' . $category . ' defined';
  }
  $title = 'Web sites ' . $name . ' (' . $r->rowCount() . ')';

  $page = [];
  $page['other-link'] = [
    '#markup' => '<div>' . l(t('Complete list of web sites and web server hosts'), 'uw_site_database/' . $category . '/' . $code) . '</div>',
  ];
  $page['list'] = [
    '#theme' => 'uw_site_database_sites_index',
    '#query_results' => $r,
    '#options' => 'a11y',
  ];

  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('UW Site Database'), 'uw_site_database');
  $breadcrumb[] = l(t('Accessibility'), 'uw_site_database/accessibility');
  if ($category === 'unit') {
    $unit_info = uw_site_database_query_unit($code);
    $group_code = isset($unit_info->group_code) ? $unit_info->group_code : 'NULL';
    $breadcrumb[] = l($group_code, 'uw_site_database/accessibility/group/' . $group_code);
  }
  drupal_set_breadcrumb($breadcrumb);

  drupal_set_title($title);
  return $page;
}
