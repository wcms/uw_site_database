<?php

/**
 * @file
 * Functions related to editing hosts.
 */

/**
 *
 */
function uw_site_database_edit($site) {
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('UW Site Database'), 'uw_site_database');

  if ($site === 'new') {
    $site = NULL;
    drupal_set_title('New host');
  }
  else {
    $q = 'SELECT *
      FROM web_host
        LEFT JOIN uw_unit USING(unit_code)
        LEFT JOIN uw_group USING(group_code)
      WHERE host_name = :host_name';
    $a = array('host_name' => $site);
    $site = uw_site_database_query($q, $a)->fetch();
    if (!$site) {
      return drupal_not_found();
    }

    $group_code = $site->group_code ?: 'NULL';
    $breadcrumb[] = l($group_code, 'uw_site_database/group/' . $group_code);
    $unit_code = $site->unit_code ?: 'NULL';
    $breadcrumb[] = l($unit_code, 'uw_site_database/unit/' . $unit_code);

    drupal_set_title('Edit: ' . l($site->host_name, 'http://' . $site->host_name), PASS_THROUGH);
  }

  drupal_set_breadcrumb($breadcrumb);

  return drupal_get_form('uw_site_database_edit_form', $site);
}

/**
 *
 */
function uw_site_database_edit_form($form, &$form_state, $site) {
  $form = array();
  $form['#submit'] = array('uw_site_database_edit_form_submit');

  $fields = array(
    'host_name' => 'Host Name',
    'dns' => 'DNS Status',
    'dns_result' => 'DNS Result',
    'duplicated_by' => 'Duplicated by',
    'duplicate_of' => 'Duplicate of',
    'sites' => 'Sites on this host',
    // Select.
    'unit_code' => 'Owner unit',
    'hosted_by_unit_code' => 'Hosted-by unit',
    'site_contact' => 'Contact WatIAM ID',
    'type' => 'Type',
    'redirecting_to' => 'Redirecting to',
    'notes' => 'Notes',
  );
  foreach ($fields as $key => $title) {
    $form[$key] = array(
      '#title' => $title,
      '#type' => 'textfield',
      '#maxlength' => 256,
      '#default_value' => isset($site->$key) ? $site->$key : NULL,
    );
  }

  $form['site_contact']['#description'] = uw_site_database_watiam_edit_description($form['site_contact']['#default_value']);
  $form['site_contact']['#element_validate'][] = 'uw_site_database_watiam_validate';

  // For existing, host_name cannot be changed and DNS status is fixed. No DNS
  // Status for new.
  if ($site) {
    $form['host_name']['#type'] = 'value';

    foreach (['dns', 'dns_result'] as $field) {
      $form[$field]['#type'] = 'item';
      $form[$field]['#markup'] = $form[$field]['#default_value'];
    }

    // Show duplicated_by.
    $q = 'SELECT host_name
      FROM web_host
      WHERE duplicate_of = :duplicate_of';
    $a = array('duplicate_of' => $site->host_name);
    $duplicated_by = uw_site_database_query($q, $a)->fetchCol();
    if ($duplicated_by) {
      $form['duplicated_by']['#type'] = 'item';
      $duplicated_by = array_map('uw_site_database_format_host_name_link', $duplicated_by);
      $form['duplicated_by']['#markup'] = implode(', ', $duplicated_by);
    }
    else {
      unset($form['duplicated_by']);
    }

    // Show sites.
    $q = 'SELECT *
      FROM web_site
      WHERE host_name = :host_name
      ORDER BY site_name';
    $a = array('host_name' => $site->host_name);
    $sites = uw_site_database_query($q, $a)->fetchAll();
    if ($sites) {
      foreach ($sites as &$site) {
        $site = uw_site_database_format_site_name_link($site);
      }
      $form['sites']['#type'] = 'ul';
      $form['sites']['#theme'] = 'item_list';
      $form['sites']['#items'] = $sites;
    }
    else {
      unset($form['sites']);
    }
  }
  else {
    foreach (['dns', 'dns_result', 'duplicated_by', 'sites'] as $field) {
      unset($form[$field]);
    }
  }

  if ($form['duplicate_of']['#default_value']) {
    $form['duplicate_of']['#description'] = uw_site_database_format_host_name_link($form['duplicate_of']['#default_value']);
  }

  foreach (['unit_code', 'hosted_by_unit_code'] as $field) {
    $form[$field]['#type'] = 'select';
    $form[$field]['#options'] = uw_site_database_get_unit_names();
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 *
 */
function uw_site_database_edit_form_submit($form, &$form_state) {
  require 'uw_site_database_update.inc';

  $update = array();
  _uw_site_database_form_submit_recurs($form, $update);

  if ($update && $form['host_name']['#type'] === 'value') {
    dsm($update, 'Updated ' . $form['host_name']['#value']);

    Database::getConnection('default', 'sitestatus')->update('web_host')
      ->fields($update)
      ->condition('host_name', $form['host_name']['#value'], '=')
      ->execute();
  }
  elseif ($update) {
    uw_site_database_host_new($form['host_name']['#value'], $update);
    drupal_goto('uw_site_database/edit/' . $form['host_name']['#value']);
  }
  else {
    drupal_set_message('No changes requested.', 'status');
  }
}

/**
 * Insert a new host record in the database.
 *
 * @param string $host_name
 *   The host name to insert.
 * @param array $update
 *   An array of other fields to set.
 *
 * @return int|null
 *   The return value from InsertQuery::execute().
 */
function uw_site_database_host_new($host_name, array $update = []) {
  $domain_parts = array_reverse(explode('.', $host_name));
  foreach (range(0, 4) as $num) {
    $key = ($num === 0 ? 't' : ($num + 1)) . 'ld';
    $update[$key] = isset($domain_parts[$num]) ? $domain_parts[$num] : NULL;
  }
  foreach (['first_seen_date', 'last_seen_date'] as $key) {
    $update[$key] = date('Y-m-d');
  }

  $dns = uw_site_database_get_dns_status($host_name);
  if (is_array($dns['dns_record'])) {
    $dns['dns_record'] = implode("\n", $dns['dns_record']);
  }
  $update['dns'] = $dns['status'];
  $update['dns_result'] = $dns['dns_record'];

  dsm($update, 'Created ' . $host_name);

  return Database::getConnection('default', 'sitestatus')->insert('web_host')
    ->fields($update)
    ->execute();
}
