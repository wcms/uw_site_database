<?php

/**
 * @file
 * Functions related to editing sites.
 */

/**
 *
 */
function uw_site_database_sites_edit($id) {
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('UW Site Database'), 'uw_site_database');

  if ($id === 'new') {
    $site = FALSE;
    drupal_set_title('New site');
  }
  else {
    $q = 'SELECT *
      FROM web_site
        LEFT JOIN uw_unit USING(unit_code)
        LEFT JOIN uw_group USING(group_code)
      WHERE id = :id';
    $a = array('id' => (int) $id);
    $site = uw_site_database_query($q, $a)->fetch();
    if (!$site) {
      return drupal_not_found();
    }

    $group_code = $site->group_code ?: 'NULL';
    $breadcrumb[] = l($group_code, 'uw_site_database/group/' . $group_code);
    $breadcrumb[] = l($site->unit_code, 'uw_site_database/unit/' . $site->unit_code);

    drupal_set_title('Edit: ' . $site->site_name);
  }

  drupal_set_breadcrumb($breadcrumb);

  return drupal_get_form('uw_site_database_sites_edit_form', $site);
}

/**
 * Return an array of status options from table wcms_site_status.
 *
 * @return string[]
 *   Array in the form status_id => status_title.
 */
function uw_site_database_get_status_options(): array {
  static $options;
  if (isset($options)) {
    return $options;
  }

  $options = [];
  $q = 'SELECT status_id, status_title FROM wcms_site_status ORDER BY sort, status_title';
  $r = uw_site_database_query($q);
  while ($row = $r->fetch()) {
    $options[$row->status_id] = $row->status_title;
  }
  return $options;
}

/**
 *
 */
function uw_site_database_sites_edit_form($form, &$form_state, $site) {
  $form = array();
  $form['#submit'] = array('uw_site_database_sites_edit_form_submit');

  foreach (uw_site_database_get_fields_site() as $key => $title) {
    $form[$key] = array(
      '#title' => $title,
      '#type' => 'textfield',
      '#default_value' => $site ? $site->$key : NULL,
    );
  }

  if ($form['wcms_path']['#default_value']) {
    $form['wcms_path']['#description'] = l($form['wcms_path']['#default_value'], uw_site_database_get_wcms_url($form['wcms_path']['#default_value']));
  }
  $form['wcms_path']['#element_validate'][] = 'uw_site_database_wcms_path_validate';

  if ($form['wcms_path_short']['#default_value']) {
    $form['wcms_path_short']['#description'] = l($form['wcms_path_short']['#default_value'], uw_site_database_get_wcms_url($form['wcms_path_short']['#default_value']));
  }

  if ($form['host_name']['#default_value']) {
    $form['host_name']['#description'] = uw_site_database_format_host_name_link($form['host_name']['#default_value']);
  }

  $form['rt']['#description'] = uw_site_database_rt_link($form['rt']['#default_value']);

  $form['wcms3_rt']['#description'] = uw_site_database_rt_link($form['wcms3_rt']['#default_value']);

  if ($form['url_1']['#default_value']) {
    $form['url_1']['#description'] = l($form['url_1']['#default_value'], $form['url_1']['#default_value']);
  }
  $form['contact_email']['#description'] = uw_site_database_watiam_edit_description($form['contact_email']['#default_value']);
  $form['contact_email']['#element_validate'][] = 'uw_site_database_watiam_validate';

  $keys = [
    'update_date',
    'start_migration',
    'end_migration',
    'migration_meeting',
    'go_live',
    'wcag_compliance_check_date',
  ];
  foreach ($keys as $key) {
    $form[$key]['#description'] = t('YEAR-MM-DD');
    // NULL dates default to today, which doesn't work, so we'll just use a
    // textfield for dates.
  }

  $form['id']['#type'] = 'value';

  $form['update_date']['#description'] .= t('. Date the site was culled or WCMS path changed.');

  $form['priority']['#type'] = 'select';
  $form['priority']['#options'] = array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5);
  $form['priority']['#empty_option'] = t('(none)');

  $form['unit_code']['#type'] = 'select';
  $form['unit_code']['#options'] = uw_site_database_get_unit_names();

  $form['owner_type']['#type'] = 'select';
  // Get #options from database.
  $form['owner_type']['#options'] = [];
  $form['owner_type']['#empty_option'] = t('Unknown');
  $q = 'SELECT * FROM org_unit_type ORDER BY org_unit_type_id';
  $r = uw_site_database_query($q);
  // Options starting in ":" display in an opt group after all non-grouped
  // options.
  $grouped_options = array();
  while ($type = $r->fetch()) {
    $explode = explode(':', $type->org_unit_type_name, 2);
    if (count($explode) > 1) {
      $grouped_options[$explode[0]][$type->org_unit_type_id] = $explode[1];
    }
    else {
      $form['owner_type']['#options'][$type->org_unit_type_id] = $type->org_unit_type_name;
    }
  }
  $form['owner_type']['#options'] += $grouped_options;
  unset($grouped_options);

  $form['private']['#type'] = 'checkbox';

  $form['status']['#type'] = 'select';
  $form['status']['#options'] = [];
  $form['status']['#empty_option'] = t('Unknown');
  $form['status']['#options'] = uw_site_database_get_status_options();

  $form['wcms3_status']['#type'] = 'select';
  $form['wcms3_status']['#options'] = [];
  $form['wcms3_status']['#empty_option'] = t('Unknown');
  $form['wcms3_status']['#options'] = uw_site_database_get_status_options();

  $form['template']['#type'] = 'select';
  $form['template']['#options'] = [];
  $form['template']['#empty_option'] = t('Unknown');
  $q = 'SELECT template_name FROM wcms_template ORDER BY template_name';
  $r = uw_site_database_query($q);
  while ($row = $r->fetch()) {
    $form['template']['#options'][$row->template_name] = $row->template_name;
  }

  $form['wcag_compliance_level']['#type'] = 'select';
  $form['wcag_compliance_level']['#options'] = wcag_compliance_levels();
  $form['wcag_compliance_level']['#empty_option'] = 'Unknown';

  $form['wcag_compliance_work_required']['#type'] = 'select';
  $form['wcag_compliance_work_required']['#options'] = [];
  $form['wcag_compliance_work_required']['#empty_option'] = t('Unknown');
  $q = 'SELECT name FROM wcms_wcag_compliance_work_required_option ORDER BY sort, name';
  $r = uw_site_database_query($q);
  while ($row = $r->fetch()) {
    $form['wcag_compliance_work_required']['#options'][$row->name] = $row->name;
  }

  $form['notes']['#type'] = 'textarea';

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update site'),
  );

  if (!$site) {
    $form['id']['#default_value'] = 'new';
    $form['submit']['#value'] = 'Save new site';
  }

  // Display site makefile.
  $site_makefile = isset($site->wcms_path) ? uw_site_database_query_site_makefile($site->wcms_path) : NULL;
  if ($site_makefile) {
    $other_use = uw_site_database_query_site_project_use($site->wcms_path);
    foreach ($other_use as $project => $sites) {
      $site_makefile[$project]['project_id'] .= ' <div class="also-used-by">Also used by ' . implode(', ', $sites) . '</div>';
    }

    $form['site_makefile'] = [
      '#theme' => 'table',
      '#caption' => t('Site makefile contents'),
      '#header' => array_keys($site_makefile[key($site_makefile)]),
      '#rows' => $site_makefile,
    ];
  }

  return $form;
}

/**
 * Return information from a site's makefile.
 *
 * @param string $site_path
 *   The URL path of the site.
 *
 * @return array[]
 *   An array keyed by project_id where the values are arrays with keys
 *   project_id, version_in_site_makefile, version_latest_7,
 *   version_in_profile_7.
 */
function uw_site_database_query_site_makefile($site_path) {
  $a = [
    'site_path' => $site_path,
  ];
  $q = 'SELECT project_id, version, version_latest_7, version_in_profile_7
    FROM {site_drupal_project}
      JOIN {drupal_project} USING (project_id)
    WHERE site_path = :site_path
    ORDER BY project_id';
  $r = uw_site_database_query($q, $a);
  $projects = [];
  while ($row = $r->fetch()) {
    $projects[$row->project_id] = [
      'project_id' => $row->project_id,
      'version_in_site_makefile' => $row->version,
      'version_latest_7' => $row->version_latest_7,
      'version_in_profile_7' => $row->version_in_profile_7,
    ];
  }
  return $projects;
}

/**
 * Return information about other sites using modules that this site uses.
 *
 * @param string $site_path
 *   The URL path of the site.
 *
 * @return array[]
 *   An array with keys of module project names and values arrays of other sites
 *   whose makefiles contain the project.
 */
function uw_site_database_query_site_project_use($site_path) {
  $a = [
    'site_path' => $site_path,
  ];
  $q = 'SELECT project_id, site_path
    FROM {site_drupal_project}
    WHERE project_id IN (SELECT project_id FROM {site_drupal_project} WHERE site_path = :site_path)
      AND site_path != :site_path
    ORDER BY project_id';
  $r = uw_site_database_query($q, $a);

  $use = [];
  if ($r) {
    while ($row = $r->fetch()) {
      $use[$row->project_id][] = $row->site_path;
    }
  }
  return $use;
}

/**
 *
 */
function uw_site_database_sites_edit_form_submit($form, &$form_state) {
  $update = array();

  // Auto-set wcag_compliance_check_date to today when wcag_compliance_level
  // changed.
  if ($form['wcag_compliance_level']['#value'] !== '' && $form['wcag_compliance_level']['#value'] !== $form['wcag_compliance_level']['#default_value'] && $form['wcag_compliance_check_date']['#value'] === '') {
    $form['wcag_compliance_check_date']['#value'] = date('Y-m-d');
  }

  _uw_site_database_form_submit_recurs($form, $update);
  dsm($update, 'Updated ' . $form['site_name']['#value']);

  // update_date: When saving the page, if the WCMS path changes or the status
  // changes to "culled", then put today's date into the field.
  if (array_key_exists('wcms_path', $update) || (isset($update['status']) && $update['status'] === 'culled')) {
    drupal_set_message(t('Update date set to today.'), 'status');
    $update['update_date'] = date('Y-m-d');
  }

  if ($update) {
    if ($form['id']['#value'] === 'new') {
      $return = Database::getConnection('default', 'sitestatus')->insert('web_site', array('return' => Database::RETURN_INSERT_ID))
        ->fields($update)
        ->execute();
      if ($return) {
        $form_state['redirect'] = 'uw_site_database/sites/edit/' . (int) $return;
      }
      return TRUE;
    }
    else {
      return Database::getConnection('default', 'sitestatus')->update('web_site')
        ->fields($update)
        ->condition('id', $form['id']['#value'], '=')
        ->execute();
    }
  }
}
