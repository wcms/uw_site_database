<?php

/**
 * @file
 * Functions related to Views.
 */

/**
 * Implements hook_views_default_views().
 */
function uw_site_database_views_default_views() {
  $view = new view();
  $view->name = 'uw_web_sites';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'web_site';
  $view->human_name = 'UW Web Sites';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'UW Web Sites';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '100';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['row_class'] = 'status-[status]';
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['columns'] = array(
    'unit_short_name' => 'unit_short_name',
    'status' => 'status',
    'id' => 'id',
    'site_name' => 'site_name',
    'unit_code' => 'unit_code',
    'contact_email' => 'contact_email',
    'rt' => 'rt',
    'go_live' => 'go_live',
    'template' => 'template',
    'migrator' => 'migrator',
    'notes' => 'notes',
  );
  $handler->display->display_options['style_options']['default'] = 'site_name';
  $handler->display->display_options['style_options']['info'] = array(
    'unit_short_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'site_name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'unit_code' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'unit_code' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'contact_email' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'rt' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'go_live' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'template' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  /* Header: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  /* Relationship: [uw_site_database] Table web_site: [uw_site_database] web_site references uw_unit ($ref_column) */
  $handler->display->display_options['relationships']['unit_code']['id'] = 'unit_code';
  $handler->display->display_options['relationships']['unit_code']['table'] = 'web_site';
  $handler->display->display_options['relationships']['unit_code']['field'] = 'unit_code';
  $handler->display->display_options['relationships']['unit_code']['label'] = 'UW Unit';
  /* Field: [uw_site_database] Table web_site: Host Name */
  $handler->display->display_options['fields']['host_name']['id'] = 'host_name';
  $handler->display->display_options['fields']['host_name']['table'] = 'web_site';
  $handler->display->display_options['fields']['host_name']['field'] = 'host_name';
  $handler->display->display_options['fields']['host_name']['exclude'] = TRUE;
  /* Field: [uw_site_database] Table web_site: Url 1 */
  $handler->display->display_options['fields']['url_1']['id'] = 'url_1';
  $handler->display->display_options['fields']['url_1']['table'] = 'web_site';
  $handler->display->display_options['fields']['url_1']['field'] = 'url_1';
  $handler->display->display_options['fields']['url_1']['label'] = 'URL';
  $handler->display->display_options['fields']['url_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['url_1']['empty'] = 'http://[host_name]/';
  /* Field: [uw_site_database] Table uw_unit: Unit Short Name */
  $handler->display->display_options['fields']['unit_short_name']['id'] = 'unit_short_name';
  $handler->display->display_options['fields']['unit_short_name']['table'] = 'uw_unit';
  $handler->display->display_options['fields']['unit_short_name']['field'] = 'unit_short_name';
  $handler->display->display_options['fields']['unit_short_name']['relationship'] = 'unit_code';
  $handler->display->display_options['fields']['unit_short_name']['exclude'] = TRUE;
  /* Field: [uw_site_database] Table web_site: Site Name */
  $handler->display->display_options['fields']['site_name']['id'] = 'site_name';
  $handler->display->display_options['fields']['site_name']['table'] = 'web_site';
  $handler->display->display_options['fields']['site_name']['field'] = 'site_name';
  $handler->display->display_options['fields']['site_name']['label'] = 'Site name';
  $handler->display->display_options['fields']['site_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['site_name']['alter']['path'] = '[url_1]';
  /* Field: [uw_site_database] Table web_site: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'web_site';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['label'] = 'Edit';
  $handler->display->display_options['fields']['id']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['id']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['id']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['id']['alter']['path'] = 'uw_site_database/sites/edit/[id]';
  $handler->display->display_options['fields']['id']['separator'] = '';
  /* Field: [uw_site_database] Table web_site: Wcms Path */
  $handler->display->display_options['fields']['wcms_path']['id'] = 'wcms_path';
  $handler->display->display_options['fields']['wcms_path']['table'] = 'web_site';
  $handler->display->display_options['fields']['wcms_path']['field'] = 'wcms_path';
  $handler->display->display_options['fields']['wcms_path']['label'] = 'WCMS Path';
  $handler->display->display_options['fields']['wcms_path']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['wcms_path']['alter']['path'] = 'https://uwaterloo.ca/[wcms_path]/';
  /* Field: [uw_site_database] Table web_site: Unit Code */
  $handler->display->display_options['fields']['unit_code']['id'] = 'unit_code';
  $handler->display->display_options['fields']['unit_code']['table'] = 'web_site';
  $handler->display->display_options['fields']['unit_code']['field'] = 'unit_code';
  $handler->display->display_options['fields']['unit_code']['label'] = 'Unit code';
  $handler->display->display_options['fields']['unit_code']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['unit_code']['alter']['text'] = '<abbr title="[unit_short_name]">[unit_code]</abbr>';
  $handler->display->display_options['fields']['unit_code']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['unit_code']['alter']['path'] = 'uw_site_database/unit/[unit_code]';
  /* Field: [uw_site_database] Table web_site: Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'web_site';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  /* Field: [uw_site_database] Table web_site: Contact Email */
  $handler->display->display_options['fields']['contact_email']['id'] = 'contact_email';
  $handler->display->display_options['fields']['contact_email']['table'] = 'web_site';
  $handler->display->display_options['fields']['contact_email']['field'] = 'contact_email';
  $handler->display->display_options['fields']['contact_email']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['contact_email']['alter']['path'] = 'mailto:[contact_email]';
  /* Field: [uw_site_database] Table web_site: Rt */
  $handler->display->display_options['fields']['rt']['id'] = 'rt';
  $handler->display->display_options['fields']['rt']['table'] = 'web_site';
  $handler->display->display_options['fields']['rt']['field'] = 'rt';
  $handler->display->display_options['fields']['rt']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['rt']['alter']['path'] = 'https://rt.uwaterloo.ca/Ticket/Display.html?id=[rt]';
  $handler->display->display_options['fields']['rt']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['rt']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['rt']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['rt']['separator'] = '';
  /* Field: [uw_site_database] Table web_site: Go Live */
  $handler->display->display_options['fields']['go_live']['id'] = 'go_live';
  $handler->display->display_options['fields']['go_live']['table'] = 'web_site';
  $handler->display->display_options['fields']['go_live']['field'] = 'go_live';
  $handler->display->display_options['fields']['go_live']['label'] = 'Go Live Date';
  /* Field: [uw_site_database] Table web_site: Template */
  $handler->display->display_options['fields']['template']['id'] = 'template';
  $handler->display->display_options['fields']['template']['table'] = 'web_site';
  $handler->display->display_options['fields']['template']['field'] = 'template';
  /* Sort criterion: [uw_site_database] Table web_site: Site Name */
  $handler->display->display_options['sorts']['site_name']['id'] = 'site_name';
  $handler->display->display_options['sorts']['site_name']['table'] = 'web_site';
  $handler->display->display_options['sorts']['site_name']['field'] = 'site_name';
  /* Filter criterion: [uw_site_database] Table web_site: Contact Email */
  $handler->display->display_options['filters']['contact_email']['id'] = 'contact_email';
  $handler->display->display_options['filters']['contact_email']['table'] = 'web_site';
  $handler->display->display_options['filters']['contact_email']['field'] = 'contact_email';
  $handler->display->display_options['filters']['contact_email']['operator'] = 'contains';
  $handler->display->display_options['filters']['contact_email']['exposed'] = TRUE;
  $handler->display->display_options['filters']['contact_email']['expose']['operator_id'] = 'contact_email_op';
  $handler->display->display_options['filters']['contact_email']['expose']['label'] = 'Contact Email';
  $handler->display->display_options['filters']['contact_email']['expose']['operator'] = 'contact_email_op';
  $handler->display->display_options['filters']['contact_email']['expose']['identifier'] = 'contact_email';
  $handler->display->display_options['filters']['contact_email']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
  );
  /* Filter criterion: [uw_site_database] Table web_site: Unit Code */
  $handler->display->display_options['filters']['unit_code']['id'] = 'unit_code';
  $handler->display->display_options['filters']['unit_code']['table'] = 'web_site';
  $handler->display->display_options['filters']['unit_code']['field'] = 'unit_code';
  $handler->display->display_options['filters']['unit_code']['exposed'] = TRUE;
  $handler->display->display_options['filters']['unit_code']['expose']['operator_id'] = 'unit_code_op';
  $handler->display->display_options['filters']['unit_code']['expose']['label'] = 'Unit Code';
  $handler->display->display_options['filters']['unit_code']['expose']['operator'] = 'unit_code_op';
  $handler->display->display_options['filters']['unit_code']['expose']['identifier'] = 'unit_code';
  $handler->display->display_options['filters']['unit_code']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
  );
  $handler->display->display_options['filters']['unit_code']['group_info']['label'] = 'Unit Code';
  $handler->display->display_options['filters']['unit_code']['group_info']['identifier'] = 'unit_code';
  $handler->display->display_options['filters']['unit_code']['group_info']['group_items'] = array(
    1 => array(
      'title' => '',
      'operator' => '=',
      'value' => '',
    ),
    2 => array(
      'title' => '',
      'operator' => '=',
      'value' => '',
    ),
    3 => array(
      'title' => '',
      'operator' => '=',
      'value' => '',
    ),
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'web-sites';

  /* Display: Web sites by faculty */
  $handler = $view->new_display('page', 'Web sites by faculty', 'page_1');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['row_class'] = 'status-[status]';
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['columns'] = array(
    'host_name' => 'host_name',
    'url_1' => 'url_1',
    'unit_short_name' => 'unit_short_name',
    'wcms_path' => 'wcms_path',
    'site_name' => 'site_name',
    'unit_code' => 'unit_code',
    'group_code' => 'group_code',
    'status' => 'status',
  );
  $handler->display->display_options['style_options']['default'] = 'wcms_path';
  $handler->display->display_options['style_options']['info'] = array(
    'host_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'url_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'unit_short_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'wcms_path' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'site_name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'unit_code' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'group_code' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: [uw_site_database] Table web_site: Host Name */
  $handler->display->display_options['fields']['host_name']['id'] = 'host_name';
  $handler->display->display_options['fields']['host_name']['table'] = 'web_site';
  $handler->display->display_options['fields']['host_name']['field'] = 'host_name';
  $handler->display->display_options['fields']['host_name']['exclude'] = TRUE;
  /* Field: [uw_site_database] Table web_site: Url 1 */
  $handler->display->display_options['fields']['url_1']['id'] = 'url_1';
  $handler->display->display_options['fields']['url_1']['table'] = 'web_site';
  $handler->display->display_options['fields']['url_1']['field'] = 'url_1';
  $handler->display->display_options['fields']['url_1']['label'] = 'URL';
  $handler->display->display_options['fields']['url_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['url_1']['empty'] = 'http://[host_name]/';
  /* Field: [uw_site_database] Table uw_unit: Unit Short Name */
  $handler->display->display_options['fields']['unit_short_name']['id'] = 'unit_short_name';
  $handler->display->display_options['fields']['unit_short_name']['table'] = 'uw_unit';
  $handler->display->display_options['fields']['unit_short_name']['field'] = 'unit_short_name';
  $handler->display->display_options['fields']['unit_short_name']['relationship'] = 'unit_code';
  $handler->display->display_options['fields']['unit_short_name']['exclude'] = TRUE;
  /* Field: [uw_site_database] Table web_site: Wcms Path */
  $handler->display->display_options['fields']['wcms_path']['id'] = 'wcms_path';
  $handler->display->display_options['fields']['wcms_path']['table'] = 'web_site';
  $handler->display->display_options['fields']['wcms_path']['field'] = 'wcms_path';
  $handler->display->display_options['fields']['wcms_path']['label'] = 'WCMS Path';
  $handler->display->display_options['fields']['wcms_path']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['wcms_path']['alter']['path'] = 'https://uwaterloo.ca/[wcms_path]/';
  /* Field: [uw_site_database] Table web_site: Site Name */
  $handler->display->display_options['fields']['site_name']['id'] = 'site_name';
  $handler->display->display_options['fields']['site_name']['table'] = 'web_site';
  $handler->display->display_options['fields']['site_name']['field'] = 'site_name';
  $handler->display->display_options['fields']['site_name']['label'] = 'Site name';
  $handler->display->display_options['fields']['site_name']['alter']['path'] = '[url_1]';
  /* Field: [uw_site_database] Table uw_unit: Group Code */
  $handler->display->display_options['fields']['group_code']['id'] = 'group_code';
  $handler->display->display_options['fields']['group_code']['table'] = 'uw_unit';
  $handler->display->display_options['fields']['group_code']['field'] = 'group_code';
  $handler->display->display_options['fields']['group_code']['relationship'] = 'unit_code';
  /* Field: [uw_site_database] Table web_site: Unit Code */
  $handler->display->display_options['fields']['unit_code']['id'] = 'unit_code';
  $handler->display->display_options['fields']['unit_code']['table'] = 'web_site';
  $handler->display->display_options['fields']['unit_code']['field'] = 'unit_code';
  $handler->display->display_options['fields']['unit_code']['label'] = 'Unit code';
  $handler->display->display_options['fields']['unit_code']['alter']['text'] = '<abbr title="[unit_short_name]">[unit_code]</abbr>';
  /* Field: [uw_site_database] Table web_site: Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'web_site';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  /* Field: [uw_site_database] Table web_site: Owner Type */
  $handler->display->display_options['fields']['owner_type']['id'] = 'owner_type';
  $handler->display->display_options['fields']['owner_type']['table'] = 'web_site';
  $handler->display->display_options['fields']['owner_type']['field'] = 'owner_type';
  /* Field: [uw_site_database] Table web_site: Private */
  $handler->display->display_options['fields']['private']['id'] = 'private';
  $handler->display->display_options['fields']['private']['table'] = 'web_site';
  $handler->display->display_options['fields']['private']['field'] = 'private';
  $handler->display->display_options['fields']['private']['separator'] = '';
  /* Field: [uw_site_database] Table web_site: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'web_site';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['label'] = 'Actions';
  $handler->display->display_options['fields']['id']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['id']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['id']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['id']['alter']['path'] = 'uw_site_database/sites/edit/[id]';
  $handler->display->display_options['fields']['id']['separator'] = '';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: [uw_site_database] Table web_site: Wcms Path */
  $handler->display->display_options['filters']['wcms_path']['id'] = 'wcms_path';
  $handler->display->display_options['filters']['wcms_path']['table'] = 'web_site';
  $handler->display->display_options['filters']['wcms_path']['field'] = 'wcms_path';
  $handler->display->display_options['filters']['wcms_path']['operator'] = 'not empty';
  $handler->display->display_options['path'] = 'web-site-lookup';

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: [uw_site_database] Table web_site: Host Name */
  $handler->display->display_options['fields']['host_name']['id'] = 'host_name';
  $handler->display->display_options['fields']['host_name']['table'] = 'web_site';
  $handler->display->display_options['fields']['host_name']['field'] = 'host_name';
  $handler->display->display_options['fields']['host_name']['exclude'] = TRUE;
  /* Field: [uw_site_database] Table web_site: Url 1 */
  $handler->display->display_options['fields']['url_1']['id'] = 'url_1';
  $handler->display->display_options['fields']['url_1']['table'] = 'web_site';
  $handler->display->display_options['fields']['url_1']['field'] = 'url_1';
  $handler->display->display_options['fields']['url_1']['label'] = 'URL';
  $handler->display->display_options['fields']['url_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['url_1']['empty'] = 'http://[host_name]/';
  /* Field: [uw_site_database] Table uw_unit: Unit Short Name */
  $handler->display->display_options['fields']['unit_short_name']['id'] = 'unit_short_name';
  $handler->display->display_options['fields']['unit_short_name']['table'] = 'uw_unit';
  $handler->display->display_options['fields']['unit_short_name']['field'] = 'unit_short_name';
  $handler->display->display_options['fields']['unit_short_name']['relationship'] = 'unit_code';
  $handler->display->display_options['fields']['unit_short_name']['exclude'] = TRUE;
  /* Field: [uw_site_database] Table web_site: Wcms Path */
  $handler->display->display_options['fields']['wcms_path']['id'] = 'wcms_path';
  $handler->display->display_options['fields']['wcms_path']['table'] = 'web_site';
  $handler->display->display_options['fields']['wcms_path']['field'] = 'wcms_path';
  $handler->display->display_options['fields']['wcms_path']['label'] = 'WCMS Path';
  $handler->display->display_options['fields']['wcms_path']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['wcms_path']['alter']['path'] = 'https://uwaterloo.ca/[wcms_path]/';
  /* Field: [uw_site_database] Table web_site: Site Name */
  $handler->display->display_options['fields']['site_name']['id'] = 'site_name';
  $handler->display->display_options['fields']['site_name']['table'] = 'web_site';
  $handler->display->display_options['fields']['site_name']['field'] = 'site_name';
  $handler->display->display_options['fields']['site_name']['label'] = 'Site name';
  $handler->display->display_options['fields']['site_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['site_name']['alter']['path'] = '[url_1]';
  /* Field: [uw_site_database] Table uw_unit: Group Code */
  $handler->display->display_options['fields']['group_code']['id'] = 'group_code';
  $handler->display->display_options['fields']['group_code']['table'] = 'uw_unit';
  $handler->display->display_options['fields']['group_code']['field'] = 'group_code';
  $handler->display->display_options['fields']['group_code']['relationship'] = 'unit_code';
  /* Field: [uw_site_database] Table web_site: Unit Code */
  $handler->display->display_options['fields']['unit_code']['id'] = 'unit_code';
  $handler->display->display_options['fields']['unit_code']['table'] = 'web_site';
  $handler->display->display_options['fields']['unit_code']['field'] = 'unit_code';
  $handler->display->display_options['fields']['unit_code']['label'] = 'Unit code';
  $handler->display->display_options['fields']['unit_code']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['unit_code']['alter']['text'] = '<abbr title="[unit_short_name]">[unit_code]</abbr>';
  $handler->display->display_options['fields']['unit_code']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['unit_code']['alter']['path'] = 'uw_site_database/unit/[unit_code]';
  /* Field: [uw_site_database] Table web_site: Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'web_site';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: [uw_site_database] Table web_site: Wcms Path */
  $handler->display->display_options['sorts']['wcms_path']['id'] = 'wcms_path';
  $handler->display->display_options['sorts']['wcms_path']['table'] = 'web_site';
  $handler->display->display_options['sorts']['wcms_path']['field'] = 'wcms_path';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: [uw_site_database] Table web_site: Wcms Path */
  $handler->display->display_options['filters']['wcms_path']['id'] = 'wcms_path';
  $handler->display->display_options['filters']['wcms_path']['table'] = 'web_site';
  $handler->display->display_options['filters']['wcms_path']['field'] = 'wcms_path';
  $handler->display->display_options['filters']['wcms_path']['operator'] = 'not empty';
  $handler->display->display_options['path'] = 'web-site-lookup.csv';

  /* Display: Services */
  $handler = $view->new_display('services', 'Services', 'services_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: [uw_site_database] Table web_site: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'web_site';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['label'] = 'id';
  $handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['id']['separator'] = '';
  /* Field: [uw_site_database] Table web_site: Wcms Path */
  $handler->display->display_options['fields']['wcms_path']['id'] = 'wcms_path';
  $handler->display->display_options['fields']['wcms_path']['table'] = 'web_site';
  $handler->display->display_options['fields']['wcms_path']['field'] = 'wcms_path';
  $handler->display->display_options['fields']['wcms_path']['label'] = 'wcms_path';
  $handler->display->display_options['fields']['wcms_path']['alter']['path'] = 'https://uwaterloo.ca/[wcms_path]/';
  $handler->display->display_options['fields']['wcms_path']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['wcms_path']['element_default_classes'] = FALSE;
  /* Field: [uw_site_database] Table web_site: Site Name */
  $handler->display->display_options['fields']['site_name']['id'] = 'site_name';
  $handler->display->display_options['fields']['site_name']['table'] = 'web_site';
  $handler->display->display_options['fields']['site_name']['field'] = 'site_name';
  $handler->display->display_options['fields']['site_name']['label'] = 'site_name';
  $handler->display->display_options['fields']['site_name']['alter']['path'] = '[url_1]';
  $handler->display->display_options['fields']['site_name']['element_label_colon'] = FALSE;
  /* Field: [uw_site_database] Table uw_unit: Group Code */
  $handler->display->display_options['fields']['group_code']['id'] = 'group_code';
  $handler->display->display_options['fields']['group_code']['table'] = 'uw_unit';
  $handler->display->display_options['fields']['group_code']['field'] = 'group_code';
  $handler->display->display_options['fields']['group_code']['relationship'] = 'unit_code';
  $handler->display->display_options['fields']['group_code']['label'] = 'group_code';
  $handler->display->display_options['fields']['group_code']['element_label_colon'] = FALSE;
  /* Field: [uw_site_database] Table web_site: Unit Code */
  $handler->display->display_options['fields']['unit_code']['id'] = 'unit_code';
  $handler->display->display_options['fields']['unit_code']['table'] = 'web_site';
  $handler->display->display_options['fields']['unit_code']['field'] = 'unit_code';
  $handler->display->display_options['fields']['unit_code']['label'] = 'unit_code';
  $handler->display->display_options['fields']['unit_code']['alter']['path'] = 'uw_site_database/unit/[unit_code]';
  $handler->display->display_options['fields']['unit_code']['element_label_colon'] = FALSE;
  /* Field: [uw_site_database] Table uw_unit: Unit Short Name */
  $handler->display->display_options['fields']['unit_short_name']['id'] = 'unit_short_name';
  $handler->display->display_options['fields']['unit_short_name']['table'] = 'uw_unit';
  $handler->display->display_options['fields']['unit_short_name']['field'] = 'unit_short_name';
  $handler->display->display_options['fields']['unit_short_name']['relationship'] = 'unit_code';
  $handler->display->display_options['fields']['unit_short_name']['label'] = 'unit_short_name';
  $handler->display->display_options['fields']['unit_short_name']['element_label_colon'] = FALSE;
  /* Field: [uw_site_database] Table uw_unit: Unit Full Name */
  $handler->display->display_options['fields']['unit_full_name']['id'] = 'unit_full_name';
  $handler->display->display_options['fields']['unit_full_name']['table'] = 'uw_unit';
  $handler->display->display_options['fields']['unit_full_name']['field'] = 'unit_full_name';
  $handler->display->display_options['fields']['unit_full_name']['relationship'] = 'unit_code';
  $handler->display->display_options['fields']['unit_full_name']['label'] = 'unit_full_name';
  $handler->display->display_options['fields']['unit_full_name']['element_label_colon'] = FALSE;
  /* Field: [uw_site_database] Table web_site: Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'web_site';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['label'] = 'status';
  $handler->display->display_options['fields']['status']['element_label_colon'] = FALSE;
  /* Field: [uw_site_database] Table web_site: Owner Type */
  $handler->display->display_options['fields']['owner_type']['id'] = 'owner_type';
  $handler->display->display_options['fields']['owner_type']['table'] = 'web_site';
  $handler->display->display_options['fields']['owner_type']['field'] = 'owner_type';
  $handler->display->display_options['fields']['owner_type']['label'] = '';
  $handler->display->display_options['fields']['owner_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: [uw_site_database] Table web_site: Wcms Path */
  $handler->display->display_options['sorts']['wcms_path']['id'] = 'wcms_path';
  $handler->display->display_options['sorts']['wcms_path']['table'] = 'web_site';
  $handler->display->display_options['sorts']['wcms_path']['field'] = 'wcms_path';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: [uw_site_database] Table web_site: Wcms Path */
  $handler->display->display_options['filters']['wcms_path']['id'] = 'wcms_path';
  $handler->display->display_options['filters']['wcms_path']['table'] = 'web_site';
  $handler->display->display_options['filters']['wcms_path']['field'] = 'wcms_path';
  $handler->display->display_options['filters']['wcms_path']['operator'] = 'not empty';
  /* Filter criterion: [uw_site_database] Table web_site: Private */
  $handler->display->display_options['filters']['private']['id'] = 'private';
  $handler->display->display_options['filters']['private']['table'] = 'web_site';
  $handler->display->display_options['filters']['private']['field'] = 'private';
  $handler->display->display_options['filters']['private']['operator'] = '!=';
  $handler->display->display_options['filters']['private']['value']['value'] = '1';
  $handler->display->display_options['path'] = 'all_sites';
  $translatables['uw_web_sites'] = array(
    t('Master'),
    t('UW Web Sites'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Displaying @start - @end of @total'),
    t('UW Unit'),
    t('Host Name'),
    t('URL'),
    t('http://[host_name]/'),
    t('Unit Short Name'),
    t('Site name'),
    t('Edit'),
    t('.'),
    t('WCMS Path'),
    t('Unit code'),
    t('<abbr title="[unit_short_name]">[unit_code]</abbr>'),
    t('Status'),
    t('Contact Email'),
    t('Rt'),
    t('Go Live Date'),
    t('Template'),
    t('Unit Code'),
    t('Page'),
    t('Web sites by faculty'),
    t('Group Code'),
    t('Owner Type'),
    t('Private'),
    t('Actions'),
    t('Data export'),
    t('Services'),
    t('id'),
    t('wcms_path'),
    t('site_name'),
    t('group_code'),
    t('unit_code'),
    t('unit_short_name'),
    t('unit_full_name'),
    t('status'),
  );

  $views[$view->name] = $view;

  // Return views.
  return $views;
}
