<?php

/**
 * @file
 * Functions related to Views.
 */

/**
 * Implements hook_views_data().
 */
function uw_site_database_views_data() {
  $data = array();
  $connection = 'sitestatus';
  try {
    $schema_dbobject = schema_dbobject($connection);
  }
  catch (Exception $e) {
    drupal_set_message(t('Unable to connect to Site Database backend: %message', ['%message' => $e->getMessage()]), 'error');
    return [];
  }
  // Suppress schema warnings. When checked, missing schema type warnings will
  // be suppressed.
  foreach ($schema_dbobject->inspect() as $table_name => $table_data) {
    if (preg_match('/^(uw|web)_/', $table_name)) {
      $table_data['module'] = 'uw_site_database';
      $table_data['database'] = $connection;
      views_schema_create_definition($data, $table_data);
    }
  }
  $data['web_site']['start_migration']['filter']['handler'] = 'date_views_filter_handler_simple';
  return $data;
}
