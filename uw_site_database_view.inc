<?php

/**
 * @file
 * Functions relating to viewing the database.
 */

/**
 *
 */
function uw_site_database_nav($category, $accessibility_view = FALSE) {
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('UW Site Database'), 'uw_site_database');
  drupal_set_breadcrumb($breadcrumb);

  switch ($category) {
    case 'unit':
    case 'group':
      if ($accessibility_view) {
        drupal_set_title('Units, by Group (accessibility)');
      }
      else {
        drupal_set_title('Units, by Group');
      }
      $q = "SELECT
          COALESCE(unit_code, 'NULL') AS unit_code,
          COALESCE(unit_short_name, 'No unit defined') AS unit_name,
          COALESCE(group_code, 'NULL') AS group_code,
          COALESCE(group_full_name, group_short_name, 'Not part of a group') AS group_name,
          host_count,
          site_count
        FROM (
          -- For each unit_code, select the counts of hosts and sites with that code.
          SELECT unit_code, unit_short_name, group_code,
            (SELECT COUNT(host_name) FROM web_host h WHERE u.unit_code = h.unit_code AND dns != :dns) AS host_count,
            (SELECT COUNT(site_name) FROM web_site s WHERE u.unit_code = s.unit_code) AS site_count
          FROM uw_unit u
          -- Add the counts for unit_code NULL.
          UNION
          SELECT NULL AS unit_code, NULL AS unit_short_name, NULL AS group_code,
            (SELECT COUNT(host_name) FROM web_host WHERE unit_code IS NULL AND dns != :dns) AS host_count,
            (SELECT COUNT(site_name) FROM web_site WHERE unit_code IS NULL) AS site_count
        ) i
          LEFT JOIN uw_group USING(group_code)
        WHERE
          host_count > 0 OR site_count > 0
        ORDER BY
          group_code IS NULL DESC,
          group_name,
          unit_code IS NULL DESC,
          unit_name";
      break;

    default:
      return drupal_not_found();
  }
  $a = array('dns' => 'not');
  $r = uw_site_database_query($q, $a);
  if (!$r) {
    return '';
  }

  $output = array();
  while ($row = $r->fetch()) {
    if ($accessibility_view) {
      $link = l($row->unit_name, 'uw_site_database/accessibility/unit/' . $row->unit_code);
      $group_link = l($row->group_name, 'uw_site_database/accessibility/group/' . $row->group_code);
      $output[$group_link][] = $link;
    }
    else {
      $link = l($row->unit_name . ' (' . $row->host_count . '; ' . $row->site_count . ')', 'uw_site_database/unit/' . $row->unit_code);
      $group_link = l($row->group_name, 'uw_site_database/group/' . $row->group_code);
      $output[$group_link][] = $link;
    }
  }

  $return = '';
  if (!$accessibility_view) {
    $return .= '<p>Unit Name (# hosts; # sites). Units with no sites or hosts are not shown.</p>';
  }
  foreach ($output as $group_name => $links) {
    $return .= '<h2>' . $group_name . '</h2>';
    $return .= '<ul><li>' . implode('</li><li>', $links) . '</li></ul>';
  }
  return $return;
}

/**
 * Generate the page and title for a list of hosts.
 *
 * @param string $category
 *   The type of display, usually "unit" or "group".
 * @param string $code
 *   The unit or group code or subtype of display.
 *
 * @return array|string
 *   A render array, empty string on DB query problem, or NULL for not found.
 */
function uw_site_database_hosts_page(string $category = NULL, string $code = NULL) {
  $where = array();
  $args = array();
  switch ($category) {
    case 'dns':
      $where[] = 'web_host.dns = :dns';
      $args['dns'] = $code;
      switch ($code) {
        case 'not':
          $title = 'Hosts not resolving';
          break;

        case 'redirect':
          $title = 'Hosts handled by Redirect Server';
          break;

        case 'resolve':
          $title = 'Hosts resolving, not handled by Redirect Server';
          break;

        default:
          return drupal_not_found();
      }
      break;

    case 'unit':
    case 'group':
      $where[] = 'web_host.dns != :dns';
      $args['dns'] = 'not';
      if ($code === 'NULL') {
        $where[] = 'uw_unit.' . $category . '_code IS NULL';
        $title = 'Resolving hosts with no ' . $category;
      }
      else {
        $where[] = 'uw_unit.' . $category . '_code = :code OR web_host.hosted_by_unit_code = :code';
        $args += array('code' => $code);
        $title = 'Resolving hosts in ' . $category . ' ' . uw_site_database_org_name($category, $code);
      }
      break;

    default:
      $where[] = 'web_host.dns != :dns';
      $args['dns'] = 'not';
      $title = 'All hosts except those not resolving';
  }
  if ($where) {
    $where = '(' . implode(') AND (', $where) . ')';
  }
  $r = uw_site_database_index_query($where, $args);
  if (!$r) {
    return '';
  }

  $title .= ' (' . $r->rowCount() . ')';

  $page = array();
  if (in_array($category, ['unit', 'group'], TRUE)) {
    $page['other-link'] = [
      '#markup' => '<div>' . l(t('List of web sites for accessibility review'), 'uw_site_database/accessibility/' . $category . '/' . $code) . '</div>',
    ];
  }

  $path = 'uw_site_database/edit/new';
  if (drupal_valid_path($path)) {
    $page['new_host'] = array(
      '#markup' => '<p><a href="' . url($path) . '">New host</a></p>',
    );
  }

  $page['dns_list'] = array(
    '#theme' => 'uw_site_database_dns_list',
    '#title' => $title,
    '#query_results' => $r,
  );

  return [$page, $title];
}

/**
 *
 */
function uw_site_database_index($category, $code) {
  $category = check_plain($category);
  $code = check_plain($code);

  $query = drupal_get_query_parameters();
  $display = NULL;
  if (($query['sort'] ?? NULL) === 'wcms3_status') {
    $display = 'wcms3_status';
  }

  $page = [];

  if ($display !== 'wcms3_status') {
    list($page['hosts'], $title) = uw_site_database_hosts_page($category, $code);
  }

  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('UW Site Database'), 'uw_site_database');

  if (in_array($category, array('unit', 'group'), TRUE)) {
    $name = uw_site_database_org_name($category, $code);
    if (!$name) {
      $name = "No $category defined";
    }

    $breadcrumb[] = l(t('Group'), 'uw_site_database/unit');
    if ($category === 'unit') {
      $unit_info = uw_site_database_query_unit($code);
      $group_code = isset($unit_info->group_code) ? $unit_info->group_code : 'NULL';
      $breadcrumb[] = l($group_code, 'uw_site_database/group/' . $group_code);
    }

    $title = $name;

    $params = [];
    if ($display === 'wcms3_status') {
      $title .= ': WCMS 3 migration status';
      $params['sort'] = ['wcms_site_status.sort IS NULL', 'wcms_site_status.sort'];
    }

    module_load_include('inc', 'uw_site_database', 'uw_site_database_sites_view');
    list($page['sites'],) = uw_site_database_sites_page($category, $code, $params);
  }

  drupal_set_breadcrumb($breadcrumb);

  drupal_set_title($title);

  return $page;
}

/**
 *
 */
function uw_site_database_index_query($where = FALSE, array $args = array()) {
  $q = 'SELECT web_host.host_name, COALESCE(duplicate_web_host.dns, web_host.dns) AS dns, web_host.duplicate_of, web_host.unit_code, unit_short_name, group_code, COALESCE(group_full_name, group_short_name) AS group_name, web_host.redirecting_to, web_host.type, web_host.site_contact, web_host.notes, web_host.dns_result
    FROM web_host
      LEFT JOIN uw_unit USING(unit_code)
      LEFT JOIN web_host duplicate_web_host ON (web_host.duplicate_of = duplicate_web_host.host_name)
      LEFT JOIN uw_group USING(group_code)';
  if ($where) {
    $q .= ' WHERE ' . $where;
  }
  $q .= ' ORDER BY web_host.tld, web_host.2ld, web_host.3ld, web_host.4ld, web_host.5ld';
  return uw_site_database_query($q, $args);
}

/**
 *
 */
function uw_site_database_query_sites_on_host($host) {
  $q = 'SELECT id, site_name, unit_code FROM web_site WHERE host_name = :host_name ORDER BY site_name';
  $a = array('host_name' => $host);
  return uw_site_database_query($q, $a)->fetchAll();
}

/**
 *
 */
function theme_uw_site_database_dns_list(array $variables) {
  $query_results = $variables['query_results'];
  $rows = array();
  while ($row = $query_results->fetch()) {
    // This cast must not be done in the while().
    $row = (array) $row;

    $class = ['dns-' . $row['dns']];

    $url = 'uw_site_database/edit/' . $row['host_name'];
    if (drupal_valid_path($url)) {
      $row['edit'] = l(t('edit'), $url);
    }

    if ($row['host_name']) {
      $sites = uw_site_database_query_sites_on_host($row['host_name']);
      $site_links = array();
      foreach ($sites as $site) {
        $site_links[] = uw_site_database_format_site_name_link($site);
      }
      $row['sites'] = implode(', ', $site_links);
    }

    if ($row['unit_code']) {
      $title = $row['unit_short_name'];
      if ($row['group_name']) {
        $title .= ', ' . $row['group_name'];
      }
      $row['unit_code'] = '<abbr title="' . $title . '">' . $row['unit_code'] . '</abbr>';
    }

    $row['host_name_link'] = '<a href="http://' . $row['host_name'] . '/">' . preg_replace('/\.uwaterloo\.ca$/', '<i>.uw</i>', $row['host_name']) . '</a>';

    $url = 'uw_site_database/edit/' . $row['duplicate_of'];
    $link = preg_replace('/\.uwaterloo\.ca$/', '<i>.uw</i>', $row['duplicate_of']);
    if (drupal_valid_path($url)) {
      $row['duplicate_of'] = l($link, $url, array('html' => TRUE));
    }
    else {
      $row['duplicate_of'] = $link;
    }

    if ($row['redirecting_to']) {
      $redir = '<a href="' . htmlspecialchars($row['redirecting_to']) . '">';
      if (preg_match(',^https?://(math\.)?uwaterloo\.ca/,', $row['redirecting_to'])) {
        $redir .= 'WCMS';
      }
      else {
        $redir .= 'Non-WCMS';
      }
      $redir .= '</a>';
      $row['redirecting_to'] = $redir;

      $class[] = 'redirect';
    }

    // Link CNAME records to host edit pages.
    if (preg_match('/^CNAME (.+)(\.uwaterloo.ca)$/', $row['dns_result'], $backref)) {
      $link = $backref[1] . '<i>.uw</i>';
      $url = 'uw_site_database/edit/' . $backref[1] . $backref[2];
      $row['dns_result'] = 'CNAME ' . l($link, $url, ['html' => TRUE]);
    }

    $row['contact'] = uw_site_database_watiam_view_line($row['site_contact']);

    $output = array();
    $keys = [
      'host_name_link',
      'edit',
      'dns',
      'duplicate_of',
      'unit_code',
      'redirecting_to',
      'type',
      'contact',
      'notes',
      'sites',
      'dns_result',
    ];
    foreach ($keys as $key) {
      if (array_key_exists($key, $row)) {
        $output[$key] = $row[$key];
      }
    }
    $rows[] = [
      'id' => ['host-' . $row['host_name']],
      'class' => $class,
      'data' => $output,
    ];
  }
  if (!empty($rows[0]) && is_array($rows[0])) {
    $variables = array(
      'attributes' => array('class' => array('host_name')),
      'caption' => $variables['title'],
      'header' => array_keys($rows[0]['data']),
      'rows' => $rows,
    );
    return theme('table', $variables);
  }
  else {
    return 'No hosts found.';
  }
}

/**
 *
 */
function uw_site_database_display_inconsistencies() {
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('UW Site Database'), 'uw_site_database');
  drupal_set_breadcrumb($breadcrumb);

  $q = "SELECT id, site_name, host_name, url_1
    FROM web_site
    WHERE url_1 NOT REGEXP '^https?://' || host_name || '/'";
  $r = uw_site_database_query($q);
  $r = $r->fetchAll();
  if ($r) {
    dsm($r, 'URL does not contain host name (should be none)');
  }

  $q = 'SELECT a.host_name AS a_host_name, b.host_name as b_host_name, b.duplicate_of
    FROM web_host a
    JOIN web_host b ON a.duplicate_of = b.host_name
    WHERE b.duplicate_of IS NOT NULL';
  $r = uw_site_database_query($q);
  $r = $r->fetchAll();
  if ($r) {
    dsm($r, 'Duplicate loops or chains (should be none)');
  }

  $q = 'SELECT *, CONCAT_WS(\'.\', 5ld, 4ld, 3ld, 2ld, tld) AS concat
    FROM web_host
    WHERE host_name != CONCAT_WS(\'.\', 5ld, 4ld, 3ld, 2ld, tld)';
  $r = uw_site_database_query($q);
  $r = $r->fetchAll();
  if ($r) {
    dsm($r, 'host_name is not concat of parts (should be none)');
  }

  $q = 'SELECT *, CONCAT_WS(\'.\', 3ld, 2ld, tld) AS concat
    FROM web_domain
    WHERE domain_name != CONCAT_WS(\'.\', 3ld, 2ld, tld)';
  $r = uw_site_database_query($q);
  $r = $r->fetchAll();
  if ($r) {
    dsm($r, 'domain_name is not concat of parts (should be none)');
  }

  $q = 'SELECT *
    FROM web_host
    WHERE (tld, 2ld) IN (
      SELECT DISTINCT tld, 2ld
      FROM web_domain
      WHERE 3ld IS NOT NULL
    ) AND (tld, 2ld, 3ld) NOT IN (
      SELECT DISTINCT tld, 2ld, 3ld FROM web_domain
    )';
  $r = uw_site_database_query($q);
  $r = $r->fetchAll();
  if ($r) {
    dsm($r, 'Host entries which do not match 3ld registrations (should be none)');
  }

  $q = 'SELECT a.host_name AS a_host_name, a.unit_code AS a_unit_code, b.host_name AS b_host_name, b.unit_code AS b_unit_code
    FROM web_host a
      JOIN web_host b ON a.duplicate_of = b.host_name
    WHERE a.unit_code != b.unit_code';
  $r = uw_site_database_query($q);
  $r = $r->fetchAll();
  if ($r) {
    dsm($r, 'Inconsistent unit_code between duplicate hosts (should be none)');
  }

  $q = 'SELECT site_name, web_site.unit_code AS site_unit_code, host_name, web_host.unit_code AS host_unit_code
    FROM web_site
      LEFT JOIN web_host USING (host_name)
    WHERE web_site.unit_code <> web_host.unit_code';
  $r = uw_site_database_query($q);
  $r = $r->fetchAll();
  if ($r) {
    dsm($r, 'Inconsistent unit_code between sites and hosts (some should exist)');
  }

  $q = 'SELECT group_code, group_short_name, COALESCE(group_full_name, group_short_name) AS group_full_name FROM uw_group';
  $r = uw_site_database_query($q);
  $groups_mysql = _uw_site_database_display_inconsistencies_flatten($r->fetchAll());

  $groups_api = uw_value_lists_uw_api_query('v2/codes/groups.json');
  $groups_api = _uw_site_database_display_inconsistencies_flatten($groups_api->data);

  $all_codes = array_keys($groups_api) + array_keys($groups_mysql);

  $errors = array();
  foreach ($all_codes as $code) {
    if ($groups_api[$code] !== $groups_mysql[$code]) {
      $errors[$code] = [
        'API' => $groups_api[$code],
        'MySQL' => $groups_mysql[$code],
      ];
    }
  }
  if ($errors) {
    dsm($errors, 'Groups mis-match: API vs. MySQL');
  }

  $q = 'SELECT unit_code, group_code, unit_short_name, COALESCE(unit_full_name, unit_short_name) AS unit_full_name FROM uw_unit WHERE is_uw_unit';
  $r = uw_site_database_query($q);
  $units_mysql = _uw_site_database_display_inconsistencies_flatten($r->fetchAll());

  $units_api = uw_value_lists_uw_api_query('v2/codes/units.json');
  $units_api = _uw_site_database_display_inconsistencies_flatten($units_api->data);

  $all_codes = array_keys($units_api) + array_keys($units_mysql);

  $errors = array();
  foreach ($all_codes as $code) {
    if ($units_api[$code] !== $units_mysql[$code]) {
      $errors[$code] = [
        'API' => $units_api[$code],
        'MySQL' => $units_mysql[$code],
      ];
    }
  }
  if ($errors) {
    dsm($errors, 'Units mis-match: API vs. MySQL');
  }

  return 'Done.';
}

/**
 *
 */
function _uw_site_database_display_inconsistencies_flatten($old) {
  $new = array();
  foreach ($old as $_) {
    $_ = (array) $_;
    $key = array_shift($_);
    $new[$key] = $_;
  }
  return $new;
}
