<?php

/**
 * @file
 * Functions related to database imports.
 */

/**
 *
 */
function uw_site_database_import_host_list() {
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('UW Site Database'), 'uw_site_database');
  drupal_set_breadcrumb($breadcrumb);

  // Get list of all host names currently in the database.
  $q = 'SELECT host_name FROM web_host';
  $r = uw_site_database_query($q);
  $names = array();
  while ($row = $r->fetch()) {
    $names[$row->host_name] = TRUE;
  }

  $error = $insert = $update = 0;

  // For each host, either update the last_seen_date or insert if it doesn't exist.
  $today = date('Y-m-d');
  $import_hosts = fopen(drupal_get_path('module', 'uw_site_database') . '/import-hosts.txt', 'r');
  while (($host_name = fgets($import_hosts)) !== FALSE) {
    $host_name = trim($host_name);
    $a = array();
    $a['last_seen_date'] = $today;
    if (empty($names[$host_name])) {
      $split = array_reverse(explode('.', $host_name));

      $a['host_name'] = $host_name;
      $a['tld'] = $split[0];
      $a['2ld'] = $split[1];
      $a['3ld'] = !empty($split[2]) ? $split[2] : NULL;
      $a['4ld'] = !empty($split[3]) ? $split[3] : NULL;
      $a['5ld'] = !empty($split[4]) ? $split[4] : NULL;
      $a['first_seen_date'] = $today;
      $a['hosted_by_unit_code'] = 'IST';
      $success = TRUE;
      try {
        $result = Database::getConnection('default', 'sitestatus')->insert('web_host')->fields($a)->execute();
      }
      catch (Exception $e) {
        $success = FALSE;
        drupal_set_message('Insert failed: ' . $host_name . ' (' . $e->errorInfo[2] . ')', 'error');
        $error++;
      }
      if ($success) {
        dsm($host_name, 'Insert');
        $insert++;
      }
    }
    else {
      Database::getConnection('default', 'sitestatus')->update('web_host')->fields($a)->condition('host_name', $host_name)->execute();
      dsm($host_name, 'Update');
      $update++;
    }
  }
  return "Imported. Errors: $error; Inserts: $insert; Updates: $update; Total: " . ($error + $insert + $update) . ".";
}

/**
 * Import new values to the web_domain table.
 */
function uw_site_database_import_web_domains() {
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('UW Site Database'), 'uw_site_database');
  drupal_set_breadcrumb($breadcrumb);

  // Upsert the domains.
  $error = $insert = 0;
  $import_domains = fopen(drupal_get_path('module', 'uw_site_database') . '/import-domains.txt', 'r');
  while (($domain_name = fgets($import_domains)) !== FALSE) {
    $domain_name = trim($domain_name);
    $a = array();

    $split = array_reverse(explode('.', $domain_name));

    $a['domain_name'] = $domain_name;
    $a['tld'] = $split[0];
    $a['2ld'] = $split[1];
    $a['3ld'] = !empty($split[2]) ? $split[2] : NULL;
    $success = TRUE;
    try {
      $result = Database::getConnection('default', 'sitestatus')
        ->merge('web_domain')
        ->key(['domain_name' => $a['domain_name']])
        ->insertFields($a)
        ->execute();
    }
    catch (Exception $e) {
      $success = FALSE;
      drupal_set_message('Upsert failed: ' . $domain_name . ' (' . $e->errorInfo[2] . ')', 'error');
      $error++;
    }
    if ($success) {
      dsm($domain_name, 'Insert');
      $insert++;
    }
  }
  return "Imported. Errors: $error; Upsert: $insert; Total: " . ($error + $insert) . ".";
}

/**
 *
 */
function uw_site_database_import_dns_list() {
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('UW Site Database'), 'uw_site_database');
  drupal_set_breadcrumb($breadcrumb);

  // @code
  // $q = 'SELECT host_name FROM web_host';
  // $r = uw_site_database_query($q);
  // $names = array();
  // while ($row = $r->fetch()) {
  //   $names[$row->host_name] = TRUE;
  // }
  // @endcode

  $file = fopen(drupal_get_path('module', 'uw_site_database') . '/dns.txt', 'r');
  if (!$file) {
    return 'Error opening file.';
  }
  $counter = 0;
  while (($line = fgetcsv($file, NULL, "\t")) !== FALSE) {
    if (empty($line[1])) {
      continue;
    }
    $host_name = $line[1];
    $split = explode('.', implode('.', array_reverse(explode('.', $host_name))));
    $a = array();
    $a['domain_name'] = $host_name;
    $a['tld'] = $split[0];
    $a['2ld'] = $split[1];
    $a['3ld'] = !empty($split[2]) ? $split[2] : NULL;
    // @code
    // $a['4ld'] = !empty($split[3]) ? $split[3] : NULL;
    // $a['5ld'] = !empty($split[4]) ? $split[4] : NULL;
    // @endcode
    Database::getConnection('default', 'sitestatus')->insert('web_domain')->fields($a)->execute();
    $counter++;
    dsm($a);
  }
  return 'Imported ' . $counter;
}

/**
 *
 */
function uw_site_database_import_site_list_html() {
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('UW Site Database'), 'uw_site_database');
  drupal_set_breadcrumb($breadcrumb);

  $doc = new DOMDocument();
  $doc->load(drupal_get_path('module', 'uw_site_database') . '/migration-schedule.html');
  $xpath = new DOMXPath($doc);
  $count = 0;
  $all_data = array();
  foreach ($doc->getElementsByTagName('tr') as $tr) {
    $count++;
    $data = array();
    $first = TRUE;

    $cells = $tr->getElementsByTagName('td');

    $a = $cells->item(0)->getElementsByTagName('a');
    if ($a->length) {
      $data['url_2'] = $a->item(0)->getAttribute('href');
    }
    else {
      $data['url_2'] = NULL;
    }

    $data['status_str'] = $cells->item(0)->getAttribute('class');
    $annotations = array();

    $old = array('url_2' => NULL, 'status_str' => NULL);
    foreach (array(
      'site_name',
      'notes',
      'start_migration',
      'end_migration',
      'go_live',
      'year',
      'migrator',
      'migration_meeting',
      'rt',
      'contact_name',
      'contact_email',
      'template',
      'url_1',
    ) as $num => $key) {
      $data[$key] = trim(preg_replace('/[' . chr(160) . chr(194) . '\s]+/', ' ', $cells->item($num)->nodeValue));
      $old[$key] = $data[$key];

      if (preg_match('/\[ANNOTATION:([^\]]+)\](.+)/', $data[$key], $backref)) {
        $annotations[] = trim($backref[1]);
        $data[$key] = trim($backref[2]);
      }
      if (!$data[$key]) {
        $data[$key] = NULL;
      }
    }
    if ($data['notes']) {
      array_unshift($annotations, $data['notes']);
    }
    $data['notes'] = implode("\n", $annotations);

    $affiliation = explode(' - ', $data['site_name']);
    $data['site_name'] = trim(array_pop($affiliation));
    $data['affiliation_1'] = array_shift($affiliation);
    $data['affiliation_2'] = array_shift($affiliation);

    foreach (array_keys($data) as $key) {
      if (in_array($key, array('rt', 'year'), TRUE)) {
        $data[$key] = (int) $data[$key];
      }
      else {
        $data[$key] = trim($data[$key]);
      }
      if (empty($data[$key])) {
        $data[$key] = NULL;
      }
    }

    foreach (array(
      'start_migration',
      'end_migration',
      'go_live',
      'migration_meeting',
    ) as $key) {
      $date = explode('.', $data[$key]);
      $month = empty($date[0]) ? NULL : $date[0];
      $day = empty($date[1]) ? NULL : (int) $date[1];
      $year = empty($data['year']) ? NULL : (int) $data['year'];
      if ($day && $month && $year) {
        $data[$key] = date('Y-m-d', strtotime("$day $month $year"));
      }
      else {
        $data[$key] = NULL;
      }
    }
    unset($old['year']);
    unset($data['year']);

    /*
    <td class="ce1"><p>Faculty/Department/School/Unit</p></td>
    <td class="ce1"><p>Comments</p></td>
    <td class="ce1"><p>Start Migration</p></td>
    <td class="ce1"><p>End Migration</p></td>
    <td class="ce1"><p>Go Live</p></td>
    <td class="ce1"><p>Year</p></td>
    <td class="ce1"><p>Migrator</p></td>
    <td class="ce1"><p>Migration Meeting</p></td>
    <td class="ce102"><p>RT#</p></td>
    <td class="ce1"><p>Point of Contact</p></td>
    <td class="ce1"><p>POC Email or Contact</p></td>
    <td class="ce1"><p>Template</p></td>
    <td class="ce1"><p>URL</p></td>
     */

    $all_data[] = $old;
    $all_data[] = $data;

    Database::getConnection('default', 'sitestatus')->insert('web_site')->fields($data)->execute();
  }
  return theme('table', array(
    'header' => array_keys($all_data[0]),
    'rows' => $all_data,
  ));
}

/**
 *
 */
function uw_site_database_import_site_list() {
  $file = fopen(drupal_get_path('module', 'uw_site_database') . '/migration-schedule.csv', 'r');
  if (!$file) {
    drupal_set_message('Error reading file.', 'error');
    return '';
  }
  $return = array();
  while (($row = fgetcsv($file)) !== FALSE) {

    /*
    0 Faculty/Department/School/Unit,
    1 Start Migration,
    2 End Migration,
    3 Go Live,
    4 year,
    5 Migrator,
    6 Migration Meeting,
    7 RT#,
    8 Point of Contact,
    9 POC Email or Contact,
    10 Template,
    11 url,
    12 url2
     */

    $affiliation = explode(' - ', $row[0]);
    $site_name = trim(array_pop($affiliation));
    $affiliation_1 = array_shift($affiliation);
    $affiliation_2 = array_shift($affiliation);

    if ($affiliation_1) {
      $affiliation_1 = trim($affiliation_1);
    }
    if ($affiliation_2) {
      $affiliation_2 = trim($affiliation_2);
    }
    if (!$affiliation_1) {
      $affiliation_1 = NULL;
    }
    if (!$affiliation_2) {
      $affiliation_2 = NULL;
    }

    // RT number.
    $row[7] = (int) $row[7];

    $insert = array(
      'site_name' => $site_name,
      'affiliation_1' => $affiliation_1,
      'affiliation_2' => $affiliation_2,
      // empty($row['']) ? NULL : $row[''], @todo.
      'status' => NULL,
    );

    foreach (array(
      1 => 'start_migration',
      2 => 'end_migration',
      3 => 'go_live',
      6 => 'migration_meeting',
    ) as $key => $name) {
      $date = explode('.', $row[$key]);
      $month = empty($date[0]) ? NULL : $date[0];
      $day = empty($date[1]) ? NULL : (int) $date[1];
      $year = empty($row[4]) ? NULL : (int) $row[4];
      if ($day && $month && $year) {
        $insert[$name] = date('Y-m-d', strtotime("$day $month $year"));
      }
      else {
        $insert[$name] = NULL;
      }
    }

    foreach (array(
      7 => 'rt',
      8 => 'contact_name',
      9 => 'contact_email',
      11 => 'url_1',
      12 => 'url_2',
      5 => 'migrator',
      10 => 'template',
      13 => 'notes',
    ) as $key => $name) {
      $insert[$name] = empty($row[$key]) ? NULL : $row[$key];
    }

    Database::getConnection('default', 'sitestatus')->insert('web_site')->fields($insert)->execute();

    // break;.
  }
  return 'Imported.';
}
