<?php

/**
 * @file
 * Functions related to DNS.
 */

/**
 * Update the database with the curernt DNS status of all hosts.
 */
function uw_site_database_update_dns_records() {
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('UW Site Database'), 'uw_site_database');
  drupal_set_breadcrumb($breadcrumb);

  // Iterate through all domains in the database.
  $q = 'SELECT host_name, dns, dns_result FROM web_host';
  $r = uw_site_database_query($q);
  while ($row = $r->fetch()) {
    $status = uw_site_database_get_dns_status($row->host_name);

    if (is_array($status['dns_record'])) {
      $status['dns_record'] = implode("\n", $status['dns_record']);
    }

    // Update the database to match for any record that has changed.
    if ($status['status'] !== $row->dns || $status['dns_record'] !== $row->dns_result) {
      $fields = [
        'dns' => $status['status'],
        'dns_result' => $status['dns_record'],
      ];
      dsm($status['dns_record'], $row->host_name . ' (old: ' . $row->dns . '; new: ' . $status['status'] . ')');
      Database::getConnection('default', 'sitestatus')->update('web_host')
        ->fields($fields)
        ->condition('host_name', $row->host_name, '=')
        ->execute();
    }
  }
  return 'Updated';
}

/**
 * Do a DNS lookup and return how the domain is resolving.
 *
 * @param string $hostname
 *   The host name.
 *
 * @return array
 *   An array with keys:
 *    - status: One of "redirect", "resolve", or "not".
 *    - dns_record: An array of strings like "A 1.2.3.4" or "CNAME example.com".
 */
function uw_site_database_get_dns_status($hostname) {
  $dns_get_record = @dns_get_record($hostname, DNS_CNAME);
  $resolving = 'not';
  if ($dns_get_record === FALSE) {
    // SERVFAIL.
  }
  elseif ($dns_get_record) {
    sort($dns_get_record);
    if ($dns_get_record[0]['target'] === 'wms-redir1.uwaterloo.ca') {
      $resolving = 'redirect';
    }
    else {
      $resolving = 'resolve';
    }
  }
  else {
    $dns_get_record = dns_get_record($hostname, DNS_A);
    if ($dns_get_record === FALSE) {
      drupal_set_message('Error host: ' . $hostname, 'error');
    }
    if ($dns_get_record) {
      sort($dns_get_record);
      if ($dns_get_record[0]['ip'] === '129.97.128.216') {
        $resolving = 'redirect';
      }
      else {
        $resolving = 'resolve';
      }
    }
  }

  if ($resolving === 'not') {
    $dns_record = NULL;
  }
  else {
    $dns_record = [];
    foreach ($dns_get_record as $record) {
      $dns_record[] = $record['type'] . ' ' . (isset($record['target']) ? $record['target'] : $record['ip']);
    }
    sort($dns_record);
  }

  return ['status' => $resolving, 'dns_record' => $dns_record];
}
