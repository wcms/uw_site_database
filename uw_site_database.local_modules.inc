<?php

/**
 * @file
 * Functions related to local modules and site makefiles.
 */

/**
 * Page callback.
 */
function uw_site_database_local_modules() {
  $page = [];

  $page['message'] = [
    '#prefix' => '<p>',
    '#markup' => t('To update this information, run makefile-update-module-db.php from uw_wcms_tools.'),
    '#suffix' => '</p>',
  ];

  $query = 'SELECT DISTINCT site_path
    FROM {site_drupal_project}
      LEFT JOIN {web_site} ON ({site_drupal_project}.site_path = {web_site}.wcms_path)
    WHERE {web_site}.wcms_path IS NULL
      AND status != :status
    ORDER BY site_path';
  $sites = [];
  $arguments = ['status' => 'culled'];
  $query = uw_site_database_query($query, $arguments);
  while ($row = $query->fetch()) {
    $sites[] = $row->site_path;
  }
  if ($sites) {
    $page['missing-sites'] = [
      '#title' => 'Makefiles that do not appear in the site database',
      '#theme' => 'item_list',
      '#items' => $sites,
    ];
  }

  $query = 'SELECT project_id, site_path, version, {web_site}.id, CONCAT_WS(\', \', version_in_profile_7, version_in_profile_8) AS version_in_profile, committer_latest_7, committer_latest_8
    FROM {site_drupal_project}
      LEFT JOIN {web_site} ON ({site_drupal_project}.site_path = {web_site}.wcms_path)
      LEFT JOIN {drupal_project} USING (project_id)
      WHERE status != :status
    ORDER BY project_id, site_path';
  $modules = [];
  $arguments = ['status' => 'culled'];
  $query = uw_site_database_query($query, $arguments);
  while ($row = $query->fetch()) {
    $path = 'uw_site_database/sites/edit/' . $row->id;
    $show_link = $row->id && drupal_valid_path($path);
    $link_text = $row->site_path ?: 'Home';
    $link_text .= ' (' . $row->version . ')';
    $modules[$row->project_id]['sites'][] = $show_link ? l($link_text, $path) : $link_text;
    $modules[$row->project_id]['version_in_profile'] = $row->version_in_profile;

    // Committer column: Show whether the committer of the latest tag in D7 and
    // D8 was WCMS team, other UW, or non-UW.
    $committer_latest = [];
    foreach ([7, 8] as $version) {
      $email = $row->{'committer_latest_' . $version};
      // Skip non-email addresses.
      if (strpos($email, '@') === FALSE) {
        continue;
      }
      list($user, $domain) = explode('@', $email, 2);
      if (substr($domain, -12) !== 'uwaterloo.ca') {
        $committer_latest[] = 'Non-UW';
      }
      elseif (uw_ldap_attribute_memberOf(variable_get('uw_auth_wcms_admins_ldap_group_admin'), $user)) {
        $committer_latest[] = 'WCMS';
      }
      else {
        $committer_latest[] = 'UW';
      }
    }
    $modules[$row->project_id]['committer'] = implode(', ', array_unique($committer_latest));
  }
  $header = [
    'Module',
    'In profile',
    'Latest committer',
    'Sites',
  ];
  $rows = [];
  foreach ($modules as $module => $module_data) {
    $row = [
      $module,
      $module_data['version_in_profile'],
      $module_data['committer'],
      theme('item_list', ['items' => $module_data['sites']]),
    ];
    $rows[] = $row;
  }
  $page['modules'] = [
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  ];

  return $page;
}
